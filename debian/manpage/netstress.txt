NAME
  netstress - utility designed to stress and benchmark network activity
SYNOPSIS
  netstress -m[NAME] | --master=[NAME] [OPTIONS]
DESCRIPTION
  Client/server utility designed to stress & benchmark network activity of
  a given ethernet device or path using simulated (random) real world data
  and packet sizes instead of fixed data and packet sizes.

  Easily command line usage with random packets sizes and random data for
  read, write, or read & write (default) testing and reporting benchmark
  results from the ethernet device.
OPTIONS
  -m[NAME], --master[=NAME]    This instance is the master
  -s[NAME], --slave[=NAME]     This instance is the slave
  -h, --host nnn.nnn.nnn.nnn   Use this host or IP address
  -p, --port nnn     Use this port (default=5678)
  --seed, -S nnn     Use this random number seed (master only)
  -c, --ctime        Report current time (default reports elapsed time)
  -n, --nsec nnn     Report every nnn seconds (default=5)
  -N, --nrep nnn     Benchmark every nnn reports (default=3)
  -b, --bytes        Benchmark using bytes (default is bits)
  -M, --mps          Benchmark using mbytes/sec or mbits/sec (default)
  -K, --kps          Benchmark using kbytes/sec or kbits/sec
  -B, --bps          Benchmark using  bytes/sec or  bits/sec
  -w, --mwo          Only test master write / slave read
  -r, --mro          Only test master read / slave write
  -R, --realrandom   Do real random test (not pseudo random)
  -f, --fast         Run Fast (no checking, no real_random)
  -v, --verbosity    Enable verbosity (multiples increase level)
  -d, --debug        Enable debugging (multiples increase level)
  -?, --help         Give this information
AUTHOR
  The NETSTRESS was written by Jim Gleason <jimgleason@users.sf.net>, Avocent Corp.
  and Helius, Inc.

  This manual page was written by Giovani Augusto Ferreira <giovani@riseup.net>
  for the Debian project (but may be used by others).
