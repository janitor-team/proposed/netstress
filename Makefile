##############################################################################
# Package: netstress Network Stress utility
# File: Makefile.arch
##############################################################################

CURDIR=$(shell basename $(shell pwd))

CFLAGS+=-O2 -Wall ${INC}
#CFLAGS+=-g -Wall ${INC}

BIN_DIR=/usr/sbin

### Currently have pkgdir/topdir nested to facilitate creating RPM's
pkgdir=pkgdir
topdir=${pkgdir}/topdir

##############################################################################

ifeq (${ARCH},)
  ARCH:=$(shell arch)
endif

ifeq (${ARCH},i686)
  ARCH:=i386
endif

ifeq (${ARCH},x86_64)
  ARCH:=amd64
endif

##############################################################################

PKGname:=$(shell grep -e "[^[:space:]]" PKGname)
PKGversion:=$(shell grep -e "[^[:space:]]" PKGversion)
PKGarch:=${ARCH}

##############################################################################

.PHONY: all
all: ${ARCH}/netstress

##############################################################################

${ARCH}/netstress: ${ARCH}/netstress.o
	${CC} ${CFLAGS} -o $@ $^

##############################################################################

${ARCH}/%.o: %.c
	@mkdir -v -p ${ARCH}
	${CC} ${CFLAGS} -c -o $@ $^

##############################################################################

.PHONY: bin_install
bin_install: ${ARCH}/netstress
	@if [ -d "${pkgdir}" ]; then rm -fr ${pkgdir}; fi
	@mkdir -p --verbose ${topdir}
	@#
	@install -v -d -m 755 ${topdir}/${INSTALL_DIR}
	@install -v -d -m 755 ${topdir}/${BIN_DIR}
	@install -v -m 755 ${ARCH}/netstress ${topdir}/${BIN_DIR}/

##############################################################################

.PHONY: deb_install
deb_install:
	${MAKE} bin_install
	@# Place the debian package information/scripts in place
	@install -v -d -m 755 ${topdir}/DEBIAN
	@install -v -m 644 DEBIAN/control ${topdir}/DEBIAN/control
	@#
	@# Replace PKGname, PKGarch and PKGversion in the control file
	sed -i	-e"s/^Package:.*/Package: ${PKGname}/g" \
		-e"s/^Version:.*/Version: ${PKGversion}/g" \
		-e"s/^Architecture:.*/Architecture: ${PKGarch}/g" \
		${topdir}/DEBIAN/control
		
##############################################################################

.PHONY: rpm_install
rpm_install:
	${MAKE} bin_install
	@# Place the redhat package information/scripts in place
	@# TBD

##############################################################################

DEB_PKG:="${ARCH}/${PKGname}_${PKGversion}_${PKGarch}.deb"

.PHONY: deb debian
deb: ${DEB_PKG}
debian: ${DEB_PKG}

${DEB_PKG}:
	${MAKE} deb_install
	@# Create the ${topdir}/DEBIAN/md5sums file
	@cd ${topdir}; \
	find . ! -path \*/DEBIAN/\* -type f | xargs md5sum > DEBIAN/md5sums
	@#
	@# Create the actual .deb package
	dpkg-deb -b ${topdir} ${DEB_PKG}
	@ls -l ${DEB_PKG}

##############################################################################

RPM_PKG:="${ARCH}/${PKGname}_${PKGversion}_${PKGarch}.rpm"

.PHONY: rpm redhat
rpm: ${DEB_PKG}
redhat: ${DEB_PKG}

${RPM_PKG}:
	${MAKE} rpm_install
	@# TBD

##############################################################################

BINTAR_PKG:=${ARCH}/${PKGname}_${PKGversion}_${ARCH}.tgz

.PHONY: bintar
bintar: ${BINTAR_PKG}

${BINTAR_PKG}:
	${MAKE} bin_install
	cd ${topdir}; \
	tar -czv -f ../../$@ .

##############################################################################

SRCTAR_PKG:=${PKGname}_${PKGversion}_src.tgz

.PHONY: srctar
srctar: ${SRCTAR_PKG}

${SRCTAR_PKG}:
	cd ..; \
	tar -czv -X ${CURDIR}/tar-excludes -f ${CURDIR}/$@ ${CURDIR}

##############################################################################

.PHONY: done
done:
	echo ""
	@find . | grep -e"\.tgz$$" -e"\.deb$$" -e"\.rpm$$" | xargs ls -lh

.PHONY: package
package:
	${MAKE} debian
	${MAKE} redhat
	${MAKE} bintar
	${MAKE} srctar
	${MAKE} done

##############################################################################

.PHONY: clean
clean:
	-@if [ -d "${ARCH}" ]; then rm -fr ${ARCH}; fi
	-@find . -mindepth 1 -maxdepth 1 -name "*.tgz" | xargs rm -f
	-@find . -mindepth 1 -maxdepth 1 -name "*.rpm" | xargs rm -f
	-@find . -mindepth 1 -maxdepth 1 -name "*.deb" | xargs rm -f
	-@rm -fr pkgdir

.PHONY: cleanall
cleanall: clean
	-@for THIS_ARCH in 1386 amd64; do \
		if [ -d "$${THIS_ARCH}" ]; then rm -fr $${THIS_ARCH}; fi; \
	done

##############################################################################

